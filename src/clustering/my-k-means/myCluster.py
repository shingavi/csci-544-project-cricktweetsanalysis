#!/usr/bin/python3
# CLUSTERING

import sys
from collections import defaultdict,Counter
import math
from nltk.corpus import stopwords
import random
import json

MY_STOP_WORDS = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'rt','they', 'them', 'their', 'theirs', 'themselves', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'other', 'some', 'such', 'no', 'nor', 'not', 'only','same', 'so', 'than', 'too', 's', 't','don', 'should', 'now']


def removeStopWords(tweetTextSplit):
	for stopWord in MY_STOP_WORDS:
		if(stopWord in tweetTextSplit):
			tweetTextSplit.remove(stopWord)
	return tweetTextSplit

def magnitude(tfidf_matrix,tweetId):
	magnitude = math.sqrt(dotProduct(tfidf_matrix,tweetId))
	return magnitude

def magnitude_centroid(centroid):
	magnitude = math.sqrt(dotProduct_centroid(centroid))
	return magnitude


def dotProduct(tfidf_matrix,tweetId):
	dotProductValue = 0.0
	for token in tfidf_matrix[tweetId].keys():
		dotProductValue += tfidf_matrix[tweetId][token]*tfidf_matrix[tweetId][token]
	return dotProductValue

def dotProduct_tweetAndCentroid(tfidf_matrix, tweetId, centroid):
	dotProductValue = 0.0
	for token in centroid.keys():
		dotProductValue += tfidf_matrix[tweetId][token]*centroid[token]
	return dotProductValue

def dotProduct_centroid(centroid):
	dotProductValue = 0.0
	for token in centroid.keys():
		dotProductValue += centroid[token]*centroid[token]
	return dotProductValue

def getCosineSimilarityValue(tfidf_matrix, tweetId, centroid):
	cosineValue = 0.0
	magTweet = magnitude(tfidf_matrix,tweetId)
	magCentroid = magnitude_centroid(centroid)
	cosineValue = float(dotProduct_tweetAndCentroid(tfidf_matrix,tweetId,centroid)/(magTweet*magCentroid))
	# print(1-cosineValue)
	return (1-cosineValue)


def generateTDIDFVector(tweetsFileName):
	print('Calculating TF-IDF Matrix...')
	termF = defaultdict(Counter) 			# {tweet_id : {wi:tfi}}
	inverseTermF = defaultdict(Counter) 	# {wi : [list of tweet_id]}
	tfidf_matrix = defaultdict(Counter)
	localDict = Counter()
	fileHandle = open(tweetsFileName,'r')

	tweetIdTextDict = defaultdict()
	for tweetLine in fileHandle:
		tweetLineSplit = tweetLine.split(' ',1)
		tweetId = tweetLineSplit[0]
		tweetText = tweetLineSplit[1].lower()
		tweetIdTextDict[tweetId] = tweetText
		localDict = Counter()
		tweetTextSplit = tweetText.split()

		

		tweettextSplit = removeStopWords(tweetTextSplit)

		for token in tweetTextSplit:
			localDict[token]+=1
			if(token in inverseTermF.keys()):
				inverseTermF[token].append(tweetId)
			else:
				newInvDocList = []
				newInvDocList.append(tweetId)
				inverseTermF[token] = newInvDocList
		for token in tweetTextSplit:
			termF[tweetId][token] = float(localDict[token]/sum(localDict.values()))

	#print(termF,inverseTermF)

	N = len(termF.keys())

	# Generate the tfidf_matrix
	for tweetId in sorted(termF.keys()):
		for token in inverseTermF.keys():
			idfValue = math.log(N/len(inverseTermF[token]))
			tfidf_matrix[tweetId][token] = float(termF[tweetId][token]*idfValue)
	
	return (tfidf_matrix,tweetIdTextDict)

def printTFIDFMatrix(tfidf_matrix):
	for tweetId in sorted(tfidf_matrix.keys()):
		for token in tfidf_matrix[tweetId]:
			tfidfValue = tfidf_matrix[tweetId][token]
			if(tfidfValue > 0.0):
				print(tweetId,token,tfidfValue)
		print()
		print()

def findKTweets(tfidf_matrix,K,isRandom):
	#toss,wicket,boundary,win,lose
	kTweets = ['587255853725523968','587274689652768768','587289448905216000','587292801378820096','587268076292349952']
	if isRandom == True:
		kTweets = []
		for i in range(K):
			kTweets.append(random.choice(list(tfidf_matrix.keys())))

	return kTweets

def getTempTweetId(tfidf_matrix):
	tempTweetIdList = list(tfidf_matrix.keys())
	return tempTweetIdList[0]

def computeCentroid(clusterNo,kClusters,tfidf_matrix,tempTweetId):
	cluster = kClusters[clusterNo]
	N = len(cluster)
	newcentroid = defaultdict()
	for token in tfidf_matrix[tempTweetId].keys():
		sumVal = 0
		for tweetId in cluster:
			sumVal += tfidf_matrix[tweetId][token]

		newcentroid[token] = float(sumVal/N)	
		
	return newcentroid

def initClustersAndCentroids(no_of_clusters,kTweets, tfidf_matrix,tempTweetId):
	kClusters = defaultdict()
	kCentroids = defaultdict()
	for i,tweetId in enumerate(kTweets):
		cluster = [tweetId]
		kClusters[i] = cluster
		kCentroids[i] = computeCentroid(i,kClusters,tfidf_matrix,tempTweetId)

	return (kClusters, kCentroids)

def findNearestCluster(tfidf_matrix,tweetId,kCentroids):
	#print('Finding nearest cluster')
	(val,clusterNumber) = (1E6,0)
	for clusterNumberindex in kCentroids.keys():
		centroid = kCentroids[clusterNumberindex]
		cosineVal = getCosineSimilarityValue(tfidf_matrix,tweetId,centroid)
		if(cosineVal < val):
			val = cosineVal
			clusterNumber = clusterNumberindex
	#print('Found nearest cluster : ' + str(clusterNumber))
	return clusterNumber

def reComputeCentroids(kClusters,tfidf_matrix):
	newCentroid = defaultdict()
	for clusterNumber in kClusters:
		print('Recomputing centroid of cluster : ' + str(clusterNumber))
		newCentroid[clusterNumber] = computeCentroid(clusterNumber,kClusters,tfidf_matrix,getTempTweetId(tfidf_matrix))

	return newCentroid

def breakingCondition(max_iter,kCentroids,newCentroids):

	if max_iter == 0:
		return True

	for clusterNumber in kCentroids.keys():
		for token in kCentroids[clusterNumber].keys():
			if kCentroids[clusterNumber][token] != newCentroids[clusterNumber][token]:
				return False

	return True

def KMeans(tfidf_matrix, no_of_clusters, isRandomStart, max_iter):
	# Get K random/Known tweets
	kTweets = findKTweets(tfidf_matrix,no_of_clusters,isRandomStart)
	tempTweetId = getTempTweetId(tfidf_matrix)
	# Initialize the clusters
	(kClusters,kCentroids) = initClustersAndCentroids(no_of_clusters,kTweets,tfidf_matrix,tempTweetId)

	#Iterate on all the tweets till a breaking condition satisfied
	while True:
		print(max_iter)
		for tweetId in tfidf_matrix.keys():
			# Find the closest cluster
			closestCluster = findNearestCluster(tfidf_matrix,tweetId,kCentroids)
			if(tweetId not in kClusters[closestCluster]):
				kClusters[closestCluster].append(tweetId)
			
		#Recompoute the centroids for all clusters
		newCentroids = reComputeCentroids(kClusters,tfidf_matrix)
		
		max_iter -= 1	
		# Check if the centroids are same and stop
		if breakingCondition(max_iter,kCentroids,newCentroids) == True:
			break

		kCentroids = newCentroids
		
	return kClusters

def main():

	# Read the tweets.txt like file to find the tfidf
	tweetsFileName = 'tweets.txt'
	if(len(sys.argv) > 1):
		tweetsFileName = sys.argv[1]

	# Get the tweetId,tweetText dictionary
	(tfidf_matrix,tweetIdTextDict) = generateTDIDFVector(tweetsFileName)

	#getCosineSimilarityValue(tfidf_matrix,'587254413288280064','587265808025268226')
	# dotProduct(tfidf_matrix,'587254215170396161','587254283264995328')
	# magnitude(tfidf_matrix,'587254215170396161')

	no_of_clusters = 20
	init = True
	max_iter = 100

	clusters = KMeans(tfidf_matrix, no_of_clusters, init, max_iter)
	print(clusters)


	myJSONCluster = {"name" : "TWEET CLUSTERS"}
	myJSONChildren = []
	# {"name" : "cluster-0","children" : [{"name" : "This is a test tweet"}]}
	for clusterKey in sorted(clusters.keys()):
		newDict = {"name" : "cluster-"+str(clusterKey)}
		clusterList = clusters[clusterKey]
		childTweetList = []
		for tweetId in sorted(clusterList):
			tweetText = tweetIdTextDict[str(tweetId)]
			tweetDict = {"name" : (tweetId + ' : ' + tweetText)}
			childTweetList.append(tweetDict)
		newDict["children"] = childTweetList
		myJSONChildren.append(newDict)

	myJSONCluster["children"] = myJSONChildren

	json.dump(myJSONCluster,open('jsonDump.json','w'),sort_keys=True)


if __name__ == '__main__':
	main()