#!/usr/bin/python3
from __future__ import print_function
import xlrd
from collections import Counter, OrderedDict, defaultdict
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.cluster import KMeans
from sklearn.externals import joblib
import pandas, os

import matplotlib.pyplot as plt
import matplotlib as mpl

import mpld3
from mpld3 import *

from scipy.cluster.hierarchy import ward, dendrogram

from sklearn.manifold import MDS

data = xlrd.open_workbook('kunal.xls')
table = data.sheet_by_index(0)

nrows = table.nrows
ncols = table.ncols

i = 0
trainingFileWordCounter = defaultdict()
totalvocab_tokenized = []
while i < nrows :
	rowData = table.row_values(i)
	posData = table.row_values(i+2)
	sentimentRowData = table.row_values(i+1)
	className = 0
	tweetId = str(rowData[0])
	if(sentimentRowData[0] != 'D'):
		className = int(sentimentRowData[0])
		if(className == 1 or className == 2):
			className = 1
		if(className == 3 or className == 4):
			className = 2
		if(className == 6 or className == 7):
			className = 3
		if(className == 8 or className == 9):
			className = 4
			
	
	#print(sentimentRowData)
	id = 1
	if(className != 0):
		features = ''
		while(id < len(rowData)-1):
			word = str(rowData[id])
			pos = posData[id]
			#if(sentimentRowData[id] != ''):
			if(pos!='\'' and pos != '#' and pos != '$' and pos != 'U' and pos != '^' and pos != '@' and pos != 'D' and pos != '' and pos != 'P' and pos != '&'):
				features+= word + ' '
			id = id + 1
		trainingFileWordCounter[tweetId] = features.rstrip()
		totalvocab_tokenized.extend(features.rstrip().lower().split())
	i = i + 6

vocab_frame = pandas.DataFrame({'words': totalvocab_tokenized},index=totalvocab_tokenized)
print('there are ' + str(vocab_frame.shape[0]) + ' items in vocab_frame')
trainingFileWordCounter = OrderedDict(sorted(trainingFileWordCounter.items()))
# print(trainingFileWordCounter)
tweetList = []
for ind,tweetTuple in enumerate(trainingFileWordCounter):
	features = trainingFileWordCounter[tweetTuple]
	tweetList.append(tweetTuple + ' : ' + features)


# print(trainingFileWordCounter.keys())
tfidf = TfidfVectorizer(lowercase=True,stop_words='english')
tfidf_matrix = tfidf.fit_transform(trainingFileWordCounter.values())

feature_names = tfidf.get_feature_names()
# for i in range(3):
# 	print('********************************************Tweet number : ' + str(i))
# 	for col in tfs.nonzero()[1]:
# 		print(feature_names[col], " : ", tfs[i,col])

dist = 1 - cosine_similarity(tfidf_matrix)

num_clusters = 10

# km = KMeans(n_clusters=num_clusters)
km = KMeans(n_clusters=num_clusters,init='k-means++')

km.fit(tfidf_matrix)

clusters = km.labels_.tolist()

# dump values
joblib.dump(km,  'doc_cluster.pkl')

km = joblib.load('doc_cluster.pkl')
clusters = km.labels_.tolist()
# print(tweetDict)
tweets = { 'tweet': tweetList,'cluster': clusters}

frame = pandas.DataFrame(tweets, index = [clusters] , columns = ['tweet', 'cluster'])

# print(frame['cluster'].value_counts())


print(vocab_frame)

print("Top terms per cluster:")
print()
#sort cluster centers by proximity to centroid
order_centroids = km.cluster_centers_.argsort()[:, ::-1] 

for i in range(num_clusters):
    print("Cluster %d Words:" % i, end='')
    
    for ind in order_centroids[i, :6]: #replace 6 with n words per cluster
    	print(' %s' % str(vocab_frame.ix[feature_names[ind].split(' ')].values.tolist()[0][0]).encode('utf-8', 'ignore'), end=',')
    print() #add whitespace
    print() #add whitespace
    
    print("Cluster %d Tweets:" % i, end='')
    for tweet in frame.ix[i]['tweet'].values.tolist():
        print(' %s,' % tweet, end='')
    print() #add whitespace
    print() #add whitespace
    
print()
print()


MDS()

# convert two components as we're plotting points in a two-dimensional plane
# "precomputed" because we provide a distance matrix
# we will also specify `random_state` so the plot is reproducible.
mds = MDS(n_components=2, dissimilarity="precomputed", random_state=1)

pos = mds.fit_transform(dist)  # shape (n_components, n_samples)

xs, ys = pos[:, 0], pos[:, 1]
print()
print()

# #set up colors per clusters using a dict
cluster_colors = {0: '#1b9e77', 1: '#d95f02', 2: '#7570b3', 3: '#e7298a', 4: '#66a61e', 5: '#62a61e', 6: '#75a61e', 7: '#74a61a', 8: '#43a63e', 9: '#78a63e'}

# #set up cluster names using a dict
cluster_names = {0: 'Cluster 0', 1: 'Cluster 1', 2: 'Cluster 2', 3: 'Cluster 3', 4: 'Cluster 4', 5: 'Cluster 5', 6: 'Cluster 6', 7: 'Cluster 7', 8: 'Cluster 8', 9: 'Cluster 9'}

# #some ipython magic to show the matplotlib plots inline
 

# #create data frame that has the result of the MDS plus the cluster numbers and titles
# df = pandas.DataFrame(dict(x=xs, y=ys, label=clusters, title=tweetList)) 

# #group by cluster
# groups = df.groupby('label')


# # set up plot
# fig, ax = plt.subplots(figsize=(17, 9)) # set size
# ax.margins(0.05) # Optional, just adds 5% padding to the autoscaling

# #iterate through groups to layer the plot
# #note that I use the cluster_name and cluster_color dicts with the 'name' lookup to return the appropriate color/label
# for name, group in groups:
#     ax.plot(group.x, group.y, marker='o', linestyle='', ms=12, 
#             label=cluster_names[name], color=cluster_colors[name], 
#             mec='none')
#     ax.set_aspect('auto')
#     ax.tick_params(\
#         axis= 'x',          # changes apply to the x-axis
#         which='both',      # both major and minor ticks are affected
#         bottom='off',      # ticks along the bottom edge are off
#         top='off',         # ticks along the top edge are off
#         labelbottom='off')
#     ax.tick_params(\
#         axis= 'y',         # changes apply to the y-axis
#         which='both',      # both major and minor ticks are affected
#         left='off',      # ticks along the bottom edge are off
#         top='off',         # ticks along the top edge are off
#         labelleft='off')
    
# ax.legend(numpoints=1)  #show legend with only 1 point

# #add label in x,y position with the label as the film title
# for i in range(len(df)):
#     ax.text(df.ix[i]['x'], df.ix[i]['y'], df.ix[i]['title'], size=8)  

    
    
# plt.show() #show the plot
# plt.close()

#uncomment the below to save the plot if need be
#plt.savefig('clusters_small_noaxes.png', dpi=200)




#define custom toolbar location
class TopToolbar(mpld3.plugins.PluginBase):
	"""Plugin for moving toolbar to top of figure"""

	JAVASCRIPT = """
	mpld3.register_plugin("toptoolbar", TopToolbar);
	TopToolbar.prototype = Object.create(mpld3.Plugin.prototype);
	TopToolbar.prototype.constructor = TopToolbar;
	function TopToolbar(fig, props){
	    mpld3.Plugin.call(this, fig, props);
	};

	TopToolbar.prototype.draw = function(){
	  // the toolbar svg doesn't exist
	  // yet, so first draw it
	  this.fig.toolbar.draw();

	  // then change the y position to be
	  // at the top of the figure
	  this.fig.toolbar.toolbar.attr("x", 150);
	  this.fig.toolbar.toolbar.attr("y", 400);

	  // then remove the draw function,
	  // so that it is not called again
	  this.fig.toolbar.draw = function() {}
	}
	"""
	def __init__(self):
	    self.dict_ = {"type": "toptoolbar"}



#create data frame that has the result of the MDS plus the cluster numbers and titles
df = pandas.DataFrame(dict(x=xs, y=ys, label=clusters, title=tweetList)) 

#group by cluster
groups = df.groupby('label')

#define custom css to format the font and to remove the axis labeling
css = """
text.mpld3-text, div.mpld3-tooltip {
  font-family:Arial, Helvetica, sans-serif;
}

g.mpld3-xaxis, g.mpld3-yaxis {
display: none; }

svg.mpld3-figure {
margin-left: -200px;}
"""


# CLUSTER 
# Plot 
# fig, ax = plt.subplots(figsize=(14,6)) #set plot size
# ax.margins(0.03) # Optional, just adds 5% padding to the autoscaling

# #iterate through groups to layer the plot
# #note that I use the cluster_name and cluster_color dicts with the 'name' lookup to return the appropriate color/label
# for name, group in groups:
#     points = ax.plot(group.x, group.y, marker='o', linestyle='', ms=18, 
#                      label=cluster_names[name], mec='none', 
#                      color=cluster_colors[name])
#     ax.set_aspect('auto')
#     labels = [i for i in group.title]
    
#     #set tooltip using points, labels and the already defined 'css'
#     tooltip = mpld3.plugins.PointHTMLTooltip(points[0], labels,
#                                        voffset=10, hoffset=10, css=css)
#     #connect tooltip to fig
#     mpld3.plugins.connect(fig, tooltip, TopToolbar())    
    
#     #set tick marks as blank
#     ax.axes.get_xaxis().set_ticks([])
#     ax.axes.get_yaxis().set_ticks([])
    
#     #set axis as blank
#     ax.axes.get_xaxis().set_visible(False)
#     ax.axes.get_yaxis().set_visible(False)

    
# ax.legend(numpoints=1) #show legend with only one dot

# mpld3.display() #show the plot

# #uncomment the below to export to html
# html = mpld3.fig_to_html(fig)
# print(html)

linkage_matrix = ward(dist) #define the linkage_matrix using ward clustering pre-computed distances

fig, ax = plt.subplots(figsize=(100, 200)) # set size
plt.subplots_adjust(left=0.125,right=0.9,bottom=0.1,top=0.9,wspace=0.2,hspace=0.2)
ax = dendrogram(linkage_matrix, orientation="right", labels=tweetList);

plt.tick_params(\
    axis= 'x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off')

plt.tight_layout() #show plot with tight layout

#uncomment below to save figure
plt.savefig('clusters.png') #save figure as ward_clusters

plt.close()