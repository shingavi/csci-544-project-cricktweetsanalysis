#!/usr/bin/python3
import sys
import json
import glob
import os,time, codecs
from collections import Counter
import subprocess
import shlex
from collections import defaultdict
import re
import xlwt


def getPOSTags(tweetText):
	print(subprocess.call(shlex.split("java -XX:ParallelGCThreads=2 -Xmx500m -jar ark-tweet-nlp-0.3.2.jar matchTweets.txt")))
	#java -XX:ParallelGCThreads=2 -Xmx500m -jar $(dirname $0)/ark-tweet-nlp-0.3.2.jar "$@"

def getTokenizedData(tweetText):
	print(subprocess.call(shlex.split("java -XX:ParallelGCThreads=2 -Xmx500m -jar ark-tweet-nlp-0.3.2.jar --just-tokenize matchTweets.txt")))
	#java -XX:ParallelGCThreads=2 -Xmx500m -jar $(dirname $0)/ark-tweet-nlp-0.3.2.jar "$@"


def dumpRAWJSON():

	total = 1
	pathToTweetsRawDump = 'stream_data/April-12-2015:MI-KP'
	allTweetFiles = sorted(glob.glob(os.path.join(pathToTweetsRawDump, '*.txt')))
	for tweetFile in allTweetFiles:
		with open(tweetFile,'r') as tweetFileReader:
			for jsonTweetStr in tweetFileReader:
				if(jsonTweetStr != '\n'):
					jsonObj = json.loads(jsonTweetStr)
					tweetText = (jsonObj['text']).replace('\n',' ').replace('\r','')
					tweetId = jsonObj['id_str']
					if(tweetText.split()[0] != 'RT'):
						print(tweetId,tweetText)


def readFromTextDump():
	fileHandle = open('a.txt','r')
	wordCounter = Counter()

	i = 0
	j = 0
	wb = xlwt.Workbook()
	ws = wb.add_sheet("Annotation")
	
	for line in fileHandle:
		tweetDataSplit = line.split(' ',1)
		tweetId = tweetDataSplit[0]
		tweetText = tweetDataSplit[1]
		j = 0
		tweetSplit = tweetText.split()
		if(tweetSplit[0] != 'RT'):
			print(tweetId,tweetText)
			ws.write(i, j,tweetId)
			j = j + 1	
			for word in tweetText.split():
				wordCounter[word]+=1
				ws.write(i, j, word)
				j = j + 1
			i = i + 2
				
	wb.save("annotation"+time.strftime('%Y-%m-%d::%H:%M:%S')+".xls")
	
def readFromDir():
	wordCounter = Counter()

	i = 0
	j = 0
	wb = xlwt.Workbook()
	total = 1
	ws = wb.add_sheet("Annotation")
	pathToTweetsRawDump = 'stream_data/April-12-2015:MI-KP/'
	allTweetFiles = sorted(glob.glob(os.path.join(pathToTweetsRawDump, '*.txt')))
	for tweetFile in allTweetFiles:	
		with codecs.open(tweetFile, "r", encoding="ISO-8859-2") as tweetFileReader:  
			for jsonTweetStr in tweetFileReader:
				if(jsonTweetStr != '\n'):
					jsonObj = json.loads(jsonTweetStr)
					tweetText = (jsonObj['text']).replace('\n',' ').replace('\r','')
					tweetId = jsonObj['id_str']
					j = 0
					if(tweetText.split()[0] != 'RT'):
						# Get the tweet POS tags for the tweetText

						print(tweetId,tweetText)
						ws.write(i, j,tweetId)
						j = j + 1	
						print(total)
						total+=1
						for word in tweetText.split():
							wordCounter[word]+=1
							ws.write(i, j, word)
							j = j + 1
						i = i + 6
	wb.save("test.xls")

def getWordFreq():
	wordCounter = Counter()
	total = 1
	pathToTweetsRawDump = 'stream_data/April-12-2015:MI-KP/'
	allTweetFiles = sorted(glob.glob(os.path.join(pathToTweetsRawDump, '*.txt')))
	for tweetFile in allTweetFiles:	
		with codecs.open(tweetFile, "r", encoding="ISO-8859-2") as tweetFileReader:  
			for jsonTweetStr in tweetFileReader:
				if(jsonTweetStr != '\n'):
					jsonObj = json.loads(jsonTweetStr)
					tweetText = (jsonObj['text']).replace('\n',' ').replace('\r','')
					tweetId = jsonObj['id_str']
					if(tweetText.split()[0] != 'RT'):
						total+=1
						for word in tweetText.split():
							wordCounter[word]+=1
	return wordCounter


def getTweetTextDict():
	fileHandler = open('matchTweets.txt','r')
	tweetTextDict = defaultdict()
	for line in fileHandler:
		tweetSplit = line.split(' ',1)
		tweetId = tweetSplit[0]
		tweetText = tweetSplit[1]
		tweetTextDict[tweetId] = tweetText
	fileHandler.close()
	return tweetTextDict

def getPOSDict():
	fileHandler = open('posTagged.txt','r')
	posDict = defaultdict()
	tweetTextDict = defaultdict()
	for line in fileHandler:
		if(line != '0\n'):
		 	posTagTextSplit = line.split('\t')
		 	posTags = posTagTextSplit[1]
		 	tweetId = (posTagTextSplit[0]).split(' ',1)[0]
		 	tweetTextDict[tweetId] = (posTagTextSplit[0]).split(' ',1)[1]
		 	posDict[tweetId] = posTags
	fileHandler.close()
	return (posDict,tweetTextDict)

def getNERDict():
	nerDict = defaultdict()
	nerDict['LOC'] = {}
	nerDict['PER'] = {}
	nerDict['VEN'] = {}
	nerDict['TEAM'] = {}
	locationFH = open('NER/LOC.txt','r')
	personFH = open('NER/PER.txt','r')
	venueFH = open('NER/VEN.txt','r')
	teamFH = open('NER/TEA.txt','r')

	for locEntity in locationFH:
		locEntity = locEntity.rstrip().lower()
		nerDict['LOC'][locEntity] = locEntity
	for perEntity in personFH:
		perEntity = perEntity.rstrip().lower()
		nerDict['PER'][perEntity] = perEntity
	for venEntity in venueFH:
		venEntity = venEntity.rstrip().lower()
		nerDict['VEN'][venEntity] = venEntity
	for teamEntity in teamFH:
		teamEntity = teamEntity.rstrip().lower()
		nerDict['TEAM'][teamEntity] = teamEntity
	return nerDict

def getEventsDict():
	eventsDict = defaultdict()
	eventsDict['TOSS'] = {}
	eventsDict['WICKETS'] = {}
	eventsDict['BOUNDARIES'] = {}
	eventsDict['MILESTONES'] = {}
	eventsDict['RESULT'] = {}

	tossFH = open('EVENTS/toss.txt','r')
	wicketsFH = open('EVENTS/wickets.txt','r')
	resultFH = open('EVENTS/result.txt','r')
	milestonesFH = open('EVENTS/milestones.txt','r')
	boundariesFH = open('EVENTS/boundaries.txt','r')

	for tossEntity in tossFH:
		tossEntity = tossEntity.rstrip().lower()
		eventsDict['TOSS'][tossEntity] = tossEntity
	for wicketsEntity in wicketsFH:
		wicketsEntity = wicketsEntity.rstrip().lower()
		eventsDict['WICKETS'][wicketsEntity] = wicketsEntity
	for boundariesEntity in boundariesFH:
		boundariesEntity = boundariesEntity.rstrip().lower()
		eventsDict['BOUNDARIES'][boundariesEntity] = boundariesEntity
	for milestonesEntity in milestonesFH:
		milestonesEntity = milestonesEntity.rstrip().lower()
		eventsDict['MILESTONES'][milestonesEntity] = milestonesEntity
	for resultEntity in resultFH:
		resultEntity = resultEntity.rstrip().lower()
		eventsDict['RESULT'][resultEntity] = resultEntity
		
	return eventsDict


def readFromDirWithPOS():
	(posDict,tweetTextDict) = getPOSDict()
	nerDict = getNERDict()
	eventsDict = getEventsDict()

	i = 0
	j = 0
	wb = xlwt.Workbook()
	total = 1
	ws = wb.add_sheet("Annotation")
	for tweetId in tweetTextDict.keys():	
		tweetStr = tweetTextDict[tweetId]
		posString = posDict[tweetId]
		posTagArr = posString.split()
		j = 0
		ws.write(i, j,tweetId)
		ws.write(i+1, j,5)
		ws.write(i+2, j,"POS")
		ws.write(i+3, j,"NER")

		j = j + 1	
		print(total)
		total+=1
		events = set()
		for idVal,word in enumerate(tweetStr.split()):
			tagVal = posTagArr[idVal+1]
			ws.write(i, j, word)
			ws.write(i+2, j, tagVal)
			if(tagVal == '^' and (word.lower() in nerDict['LOC'].keys())):
				ws.write(i+3, j,"B-LOC")
			elif((word.lower() in nerDict['TEAM'].keys())):
				ws.write(i+3, j,"B-TEAM")
			elif(tagVal == '^' and not re.match('^#',word)):
				ws.write(i+3, j,"B-PER")
			elif(re.match('^#',word)):
				ws.write(i+3, j,"B-HASH")
			else:
				ws.write(i+3, j,"O")

			if(word.lower() in eventsDict['TOSS'].keys()):
				ws.write(i+4, j,"TOSS")
				events.add("TOSS")
			elif(word.lower() in eventsDict['WICKETS'].keys()):
				ws.write(i+4, j,"WICKETS")
				events.add("WICKETS")
			elif(word.lower() in eventsDict['BOUNDARIES'].keys()):
				ws.write(i+4, j,"BOUNDARIES")
				events.add("BOUNDARIES")
			elif(word.lower() in eventsDict['MILESTONES'].keys()):
				ws.write(i+4, j,"MILESTONES")
				events.add("MILESTONES")
			elif(word.lower() in eventsDict['RESULT'].keys()):
				ws.write(i+4, j,"RESULT")
				events.add("RESULT")

			j = j + 1
		
		eventsStr = ""
		for event in events:
			eventsStr += event + " "

		ws.write(i+4, 0,eventsStr.rstrip())
		i = i + 6
	wb.save("test.xls")

def main():



	# Create a text dump ()
	#dumpRAWJSON()

	getPOSTags("NULL")

	#readFromTextDump()
	#dumpRAWJSON()
	#readFromDir()
	#readFromDirWithPOS()
	# wordCounter = getWordFreq()
	# for i,val in enumerate(wordCounter.most_common()):
	# 	print(val[0],val[1])

	
	#getTokenizedData("NULL")

if __name__ == '__main__':
	main()