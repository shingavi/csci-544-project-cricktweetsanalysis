#!/usr/bin/python3

import sys
import xlrd
from collections import defaultdict

# Convert the manual annotated tweets to excel

def writeOnlyTweets(excelFileName, fileNameToWrite):
	data = xlrd.open_workbook(excelFileName)
	fileHandle = open(fileNameToWrite,'w')
	table = data.sheet_by_index(0)
	tweetDict = defaultdict()
	nrows = table.nrows
	ncols = table.ncols

	i = 0
	while i < nrows :
		rowData = table.row_values(i)
		posData = table.row_values(i+2)
		sentimentRowData = table.row_values(i+1)
		className = 0
		tweetId = str(rowData[0])
		if(sentimentRowData[0] != 'D'):
			className = int(sentimentRowData[0])
			if(className == 1 or className == 2):
				className = 1
			if(className == 3 or className == 4):
				className = 2
			if(className == 6 or className == 7):
				className = 3
			if(className == 8 or className == 9):
				className = 4
				
		
		#print(sentimentRowData)
		id = 1
		if(className != 0):
			features = str(className) + ' '
			while(id < len(rowData)-1):
				word = str(rowData[id])
				pos = posData[id]
				#if(sentimentRowData[id] != ''):
				if(pos != '#' and pos != '$' and pos != 'U'  and pos != 'D' and pos != '' and pos != 'P' and pos != '&'):
					features+= word + ' '
				id = id + 1
			tweetDict[tweetId] = features.rstrip()
		i = i + 6


	for tweetId in sorted(tweetDict.keys()):
		features = tweetDict[tweetId]
		fileHandle.write(features + '\n')

	fileHandle.close()


def main():

	excelFileName = 'kunal_test.xls'
	fileNameToWrite = 'a.txt'
	choice = 1
	if(len(sys.argv) > 1):
		excelFileName = sys.argv[1]
	if(len(sys.argv) > 2):
		fileNameToWrite = sys.argv[2]
	if(len(sys.argv) > 3):
		choice = int(sys.argv[3])

	if(choice == 1):
		writeOnlyTweets(excelFileName, fileNameToWrite)
	else:
		print('Other choice doesn\'t exist')
	
if __name__ == '__main__':
	main()