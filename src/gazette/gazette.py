#!/usr/bin/python3
#Gazette
import sys
from collections import defaultdict,Counter
from nltk.corpus import stopwords
import random
import json

def main():

	# Read the tweets.txt like file to find the tfidf
	tweetsFileName = 'tweets.txt'
	if(len(sys.argv) > 1):
		tweetsFileName = sys.argv[1]

		#print(Tweet_words)
	Loc_fileHandle = open("NER/LOC.txt","r");
	Per_fileHandle = open("NER/PER.txt","r");
	Tea_fileHandle = open("NER/TEA.txt","r");
	Ven_fileHandle = open("NER/VEN.txt","r");

	LocDict = defaultdict(Counter)
	PerDict = defaultdict(Counter)
	TeaDict = defaultdict(Counter)
	VenDict = defaultdict(Counter)
	finalDict = defaultdict(Counter)

	for line in Loc_fileHandle:
		LocDict['LOC'][line.rstrip().lower()]+=1
	for line in Per_fileHandle:
		PerDict['PER'][(line.lower().rstrip().split()[0])]+=1
		PerDict['PER'][(line.lower().rstrip().split()[1])]+=1
	for line in Tea_fileHandle:
		TeaDict['TEA'][line.rstrip().lower()]+=1
	for line in Ven_fileHandle:
		VenDict['VEN'][line.rstrip().lower()]+=1

	Tweet_words = []
	for line in open(tweetsFileName,'r'):
		spilttext = line.split(" ",1)
		Tweet_id = spilttext[0]
		Tweet_text = spilttext[1].rstrip().lower()
		Tweet_words += Tweet_text.split()
	# print(TweetDict)	
	
	for locations in (LocDict['LOC']):
		if any(locations in s for s in Tweet_words):
			finalDict['LOCATION'][locations]+=1

	for persons in (PerDict['PER']):
		if any(persons in s for s in Tweet_words):
			finalDict['PERSON'][persons]+=1
			
	for teams in (TeaDict['TEA']):
		if any(teams in s for s in Tweet_words):
			finalDict['TEAM'][teams]+=1

	for venue in (VenDict['VEN']):
		if any(venue in s for s in Tweet_words):
			finalDict['VENUE'][venue]+=1

	#print(finalDict)

	NERJSONcluster = {"name" : "NER"}
	NERJSONChildren = []
	for nerkey in sorted(finalDict.keys()):
		newDict = {"name" : str(nerkey)}
		nerList = finalDict[nerkey]
		nerClusterList = []
		for ner in nerList:
			categoryDict = {"name" : (str(ner))}
			nerClusterList.append(categoryDict)
		newDict["children"] = nerClusterList
		NERJSONChildren.append(newDict)

	NERJSONcluster["children"] = NERJSONChildren
	json.dump(NERJSONcluster,open('jsonDump.json','w'),sort_keys=True)


if __name__ == '__main__':
	main()