#!/usr/bin/python3
import sys
import re
from collections import defaultdict,OrderedDict, Counter
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.cluster import KMeans
import pandas
import operator


MY_STOP_WORDS = ['match', 'ipl', 'team', 'i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'rt','they', 'them', 'their', 'theirs', 'themselves', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'other', 'some', 'such', 'no', 'nor', 'not', 'only','same', 'so', 'than', 'too', 's', 't','don', 'should', 'now']

def validToken(token):
	if(token[0] == '#' or token[0] == '@'):
		return False
	return True


def removeStopWords(tweetText):
	tweetTextSplit = tweetText.split()
	freshTweet = ''
	for stopWord in MY_STOP_WORDS:
		if(stopWord in tweetTextSplit):
			tweetTextSplit.remove(stopWord)
	for token in tweetTextSplit:
		if(validToken(token) == True):
			freshTweet += token + ' '
	return freshTweet.rstrip()

def parseTweetData(tweetData):
	tweetDataSplit = tweetData.split(' ',1)
	tweetId = tweetDataSplit[0]
	tweeetDataTimeStampSplit = tweetDataSplit[1].split(')')[0]
	timeStamp = (tweeetDataTimeStampSplit[1:]).split(' ')[3]
	tweetText = removeStopWords(tweetDataSplit[1].split(')')[1])
	return (tweetId,timeStamp,tweetText)


def getTweetsWithTimeStamp(fileName):
	fileHandle = open(fileName,'r')
	tweetsTimeStampDict = defaultdict(Counter)
	for tweetData in fileHandle:
		(tweetId,timeStamp,tweetText) = parseTweetData(tweetData.lower())
		tweetsTimeStampDict[timeStamp][tweetId] = tweetText
	return tweetsTimeStampDict

def getHourAndMinuteTimeStamp(timeStamp):
	timeStampSplit = timeStamp.split(':')
	hourStamp = timeStampSplit[0]
	minuteStamp = timeStampSplit[1]
	#print(hourStamp,minuteStamp)
	return (hourStamp,minuteStamp)

def getFirstTimeStamp(tweetsTimeStampDict):
	for timeStamp in sorted(tweetsTimeStampDict):
		(hourStamp,minuteStamp) = getHourAndMinuteTimeStamp(timeStamp)
		return(hourStamp,minuteStamp)


def getKWindowTweets(k,tweetsTimeStampDict):
	# Iterate over all the timestamped tweets and split them in the bucket of window size k-minutes
	(startHourStamp,startMinuteStamp) = getFirstTimeStamp(tweetsTimeStampDict)
	(currHourStamp,currMinuteStamp) = (startHourStamp,startMinuteStamp)
	#print('First timestamp : ', (currHourStamp,currMinuteStamp))
	windowSize = int(k) - 1
	chunkNumber = 0
	kWindowTweets = defaultdict(Counter)
	for timeStamp in sorted(tweetsTimeStampDict):
		(hourStamp,minuteStamp) = getHourAndMinuteTimeStamp(timeStamp)
		if(currHourStamp == hourStamp):
			if(int(minuteStamp) > (int(currMinuteStamp)+windowSize)):
				# Break the chunk
				# print('Chunk Break')
				# print(hourStamp,minuteStamp)
				currMinuteStamp = minuteStamp
				chunkNumber+=1
				if(chunkNumber in kWindowTweets):
					kWindowTweets[(chunkNumber)].append(tweetsTimeStampDict[timeStamp])
				else :
					kWindowTweets[(chunkNumber)] = [tweetsTimeStampDict[timeStamp]]
		
			else:
				# Dump the tweets of this timestamp into currentChunk
				if(chunkNumber in kWindowTweets) :
					(kWindowTweets[(chunkNumber)]).append(tweetsTimeStampDict[timeStamp])
				else :
					kWindowTweets[(chunkNumber)] = [tweetsTimeStampDict[timeStamp]]
				# print(hourStamp,minuteStamp)
		else:
			# print('Change in hour')
			# print('Chunk Break')
			# print(hourStamp,minuteStamp)
			chunkNumber+=1
			if(chunkNumber in kWindowTweets):
				kWindowTweets[(chunkNumber)].append(tweetsTimeStampDict[timeStamp])
			else :
				kWindowTweets[(chunkNumber)] = [tweetsTimeStampDict[timeStamp]]
			currHourStamp = hourStamp
			currMinuteStamp = 0

	return kWindowTweets

def printChunk(kWindowTweets):
	sumAll = 0
	for chunkNumber in sorted(kWindowTweets):
		#print('Chunk Number : ' + str(chunkNumber))
		sumAll += len(kWindowTweets[chunkNumber])
		for index,tweetBucket in enumerate(kWindowTweets[chunkNumber]):
			for tweetId in sorted(tweetBucket.keys()):
				print(index,tweetId,tweetBucket[tweetId])
		# 	print(kWindowTweets[chunkNumber][tweetId])
	#print(sumAll)
def getAllChunkTweets(currentChunk):
	chunkTweetsDict = defaultdict()
	for index,tweetBucket in enumerate(currentChunk):
		for tweetId in sorted(tweetBucket.keys()):
			chunkTweetsDict[tweetId] = tweetBucket[tweetId]
	return chunkTweetsDict
	
def getKeywords(num_clusters,feature_names,order_centroids,vocab_frame,frame):
	keywordsDict = defaultdict()
	for i in range(num_clusters):
		for ind in (order_centroids[i, :6]):
			keyword = str(vocab_frame.ix[feature_names[ind].split(' ')].values.tolist()[0][0])#.encode('utf-8', 'ignore')
			if(keyword != 'nan'):
				keywordsDict[keyword] = keyword
	return keywordsDict

	 
def getTopKSummaries(currentChunk,feature_names,num_clusters,order_centroids,vocab_frame,frame):

	keywordsDict = getKeywords(num_clusters,feature_names,order_centroids,vocab_frame,frame)
	#print(keywordsDict)
	chunkTweetsDict = getAllChunkTweets(currentChunk)
	scoreDict = defaultdict()
	for tweetId in chunkTweetsDict.keys():
		tweetText = chunkTweetsDict[tweetId]
		score = 0
		for keyword in keywordsDict.keys():
			if(keyword.lower() in tweetText.lower().split()):
				score += 1
		scoreDict[tweetId] = score
	#sorted_scoreDict = sorted(scoreDict.items(), key=operator.itemgetter(1))
	#print(scoreDict)
	topkSummariesTweetIdList = sorted(list(scoreDict))#[len(scoreDict.keys())-10:]
	# print(topkSummariesTweetIdList)
	#print(keywordsDict.keys())
	return keywordsDict.keys()
	# for topTweetId in topkSummariesTweetIdList:
	# 	print(chunkTweetsDict[topTweetId])
	
	
def getSummaryForCurrentChunk(kWindowTweets,chunkNumber):
	currentChunk = kWindowTweets[chunkNumber]
	# Generate TFIDF
	# Cluster by events
	# Find tweets with max similarity
	tweetChunkDict = defaultdict()
	totalvocab_tokenized = []
	for index,tweetBucket in enumerate(currentChunk):
				for tweetId in sorted(tweetBucket.keys()):
					tweetText = tweetBucket[tweetId]
					tweetChunkDict[tweetId] = tweetText.lower()
					totalvocab_tokenized.extend(tweetText.lower().split())
	# Sort dict
	
	trainingFileWordCounter = OrderedDict(sorted(tweetChunkDict.items()))
	vocab_frame = pandas.DataFrame({'words': totalvocab_tokenized},index=totalvocab_tokenized)
	tweetList = []
	for ind,tweetTuple in enumerate(trainingFileWordCounter):
		features = trainingFileWordCounter[tweetTuple]
		tweetList.append(tweetTuple + ' : ' + features)

	tfidf = TfidfVectorizer(lowercase=True,stop_words='english')
	tfidf_matrix = tfidf.fit_transform(trainingFileWordCounter.values())

	feature_names = tfidf.get_feature_names()

	dist = 1 - cosine_similarity(tfidf_matrix)

	num_clusters = 2

	km = KMeans(n_clusters=num_clusters,init='k-means++')

	km.fit(tfidf_matrix)

	clusters = km.labels_.tolist()
	tweets = { 'tweet': tweetList,'cluster': clusters}

	frame = pandas.DataFrame(tweets, index = [clusters] , columns = ['tweet', 'cluster'])
	#sort cluster centers by proximity to centroid
	order_centroids = km.cluster_centers_.argsort()[:, ::-1] 

	topkSummaries = getTopKSummaries(currentChunk,feature_names,num_clusters,order_centroids,vocab_frame,frame)

	#print(clusters)

	return topkSummaries



def getSummaryChunks(kWindowTweets,average):
	summaryChunks = defaultdict()
	sumTotal = 0
	for chunkNumber in sorted(kWindowTweets):
		#print('Summary For Chunk Number : ' + str(chunkNumber))
		
		if(len(kWindowTweets[chunkNumber]) >= average):
			summaryChunks[chunkNumber] = getSummaryForCurrentChunk(kWindowTweets,chunkNumber)
			print(str(chunkNumber)+'\t'+str(len(kWindowTweets[chunkNumber])) + '\t' + str(','.join([str(keyword) for keyword in summaryChunks[chunkNumber]])))
		else:
			print(str(chunkNumber)+'\t'+str(len(kWindowTweets[chunkNumber])) + '\t' + 'NA')
		#print(summaryChunks[chunkNumber])
	return (summaryChunks)

def calculateAverage(kWindowTweets):
	num_chunks = len(kWindowTweets.keys())
	sumAll = 0
	for chunkNumber in kWindowTweets.keys():
		sumAll += len(kWindowTweets[chunkNumber])

	return float(sumAll/num_chunks)

def main():

	fileNameWithTimeStamp = 'timestamp_tweets.txt'
	if(len(sys.argv) > 1):
		fileNameWithTimeStamp = sys.argv[1]
	# Get the tweets with timestamp
	tweetsTimeStampDict = getTweetsWithTimeStamp(fileNameWithTimeStamp)
	# Bucket the tweets in k-length window
	k = 3 #2 min window size
	if(len(sys.argv) > 2):
		k = sys.argv[2]
	# Find the chunks of windows for summarization
	kWindowTweets = getKWindowTweets(k,tweetsTimeStampDict)

	average = calculateAverage(kWindowTweets)
	#printChunk(kWindowTweets)
	# Find the summary for each chunk discovered
	(summaryChunks) = getSummaryChunks(kWindowTweets,average)

if __name__ == '__main__':
	main()