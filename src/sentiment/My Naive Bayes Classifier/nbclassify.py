#!/usr/bin/python3

'''The classifier uses the model file and classifies the testing file documents '''
from collections import defaultdict
import sys
import pickle
import re
import math


# Constant - PRIORS_AND_FEATURES_IN_CSCI-544_ASSIGNMENT
CONSTANT_KEY_FOR_PRIORS = 'PRIORS_AND_FEATURES_IN_CSCI544_ASSIGNMENT'
CONSTANT_KEY_FOR_TRAINING_FEATURES = 'TRAINING_FEATURES_IN_CSCI544_ASSIGNMENT'
''' 
	Function to filter out punctuations from the textContent arg
'''
def tokenize(line):
	return re.findall('[a-z0-9]+',line)


def bayesian_classify(line,defaultDictPriorsAndLikelihoods):
	'''Return the biggest count class'''
	max_categoryClass = (-1E6,'')
	uniqueNumberOfCategories = len(defaultDictPriorsAndLikelihoods[CONSTANT_KEY_FOR_PRIORS].keys())
	vocabSize = float(sum(defaultDictPriorsAndLikelihoods[CONSTANT_KEY_FOR_TRAINING_FEATURES].values()))
	for categoryClass in defaultDictPriorsAndLikelihoods[CONSTANT_KEY_FOR_PRIORS].keys():
		priorsCount = math.log(defaultDictPriorsAndLikelihoods[CONSTANT_KEY_FOR_PRIORS][categoryClass])
		#print(priorsCount)
		n = float(sum(defaultDictPriorsAndLikelihoods[categoryClass].values()))

		for word in tokenize(line):
			#priorsCount = priorsCount + math.log(max(1E-9,(defaultDictPriorsAndLikelihoods[categoryClass][word])/n))
			priorsCount = priorsCount + math.log(max(((defaultDictPriorsAndLikelihoods[categoryClass][word]+1)/(n+vocabSize)),(defaultDictPriorsAndLikelihoods[categoryClass][word])/n))
			#print('Priors count for word ',word,priorsCount)
		if priorsCount > max_categoryClass[0]:
			max_categoryClass = (priorsCount,categoryClass)

	#print(max_categoryClass)
	return(max_categoryClass[1])


def main():

	# Open the model file in binary mode to load the defaultdict object
	modelFile = open(sys.argv[1],'rb')
	# Open the testing file to read each line and classify it as SPAM or HAM
	testingFile = open(sys.argv[2],'r')

	# Load the priors and likelihood generic defaultdict
	defaultDictPriorsAndLikelihoods = pickle.load(modelFile)
	modelFile.close()

	#Print keys
	#print(defaultDictPriorsAndLikelihoods.keys())
	errCount = 0
	totalDocumentsToClassify = 0
	bayesianPredictedClassStr=''
	for line in testingFile:
		totalDocumentsToClassify +=1
		#bayesianPredictedClass = bayesian_classify(line.split(' ',1)[1],defaultDictPriorsAndLikelihoods)
		bayesianPredictedClass = bayesian_classify(line,defaultDictPriorsAndLikelihoods)
		#sys.stdout.write(bayesianPredictedClass+'\n')
		bayesianPredictedClassStr+=bayesianPredictedClass+'\n'
		'''actualClassification = line.split(' ',1)[0]
		if actualClassification != bayesianPredictedClass:
			errCount +=1'''
	sys.stdout.write(bayesianPredictedClassStr.rstrip('\n'))
	'''print(totalDocumentsToClassify)
	print(errCount)
	print('Accuracy : ')
	print((totalDocumentsToClassify - errCount)/totalDocumentsToClassify * 100)'''
	testingFile.close()

# Call main function
if __name__ == '__main__':
	main()
