#!/usr/bin/python3
from collections import Counter, defaultdict
import sys
import re
import pickle


''' 
	Function to filter out punctuations from the textContent arg
'''
def tokenize(textContent):
	return re.findall('[a-z0-9]+',textContent)

''' 
	Function to read the testing file (LABEL <FEATURES>) and 
	create the model file (defaultdict object with all the priors and likelihood counts)
'''
def getDictOfPriorsLikelihoodsFromTestFile(trainingFile):
	priorLikelihoodDefaultDictionary = defaultdict(Counter)
	vocabCounter = Counter()
	totalCount = 0
	# Read the testing file
	for sentence in trainingFile:
		labeledDocument = sentence.split(' ',1)
		label = labeledDocument[0]
		for word in tokenize(labeledDocument[1]):
			vocabCounter[word]+=1
			priorLikelihoodDefaultDictionary[label][word]+=1

		#This stores  the priors with Default constant key used below and likelihoods with key as the class (example : SPAM)
		priorLikelihoodDefaultDictionary['PRIORS_AND_FEATURES_IN_CSCI544_ASSIGNMENT'][label]+=1
		totalCount+=1
	priorLikelihoodDefaultDictionary['TRAINING_FEATURES_IN_CSCI544_ASSIGNMENT']['VOCAB_SIZE'] = len(vocabCounter.keys())
	'''print('Total number of documents : ',totalCount)'''
	return (priorLikelihoodDefaultDictionary)


# Define main function
def main():

	trainingFile = open(sys.argv[1],'r')	# Testing file with (Label <Features>)
	modelFile = open(sys.argv[2],'wb')	# Model file to store the Priors and the Likelihood

	# Read the testing file (to learn) generated earlier and dump the returned defaultdict in the modelfile
	(priorLikelihoodDefaultDictionary) = getDictOfPriorsLikelihoodsFromTestFile(trainingFile)
	
	#Write back the dictionary returned (learning model) to the model file (argv[2])
	# pickle library writes the dictionary data in binary format
	pickle.dump(priorLikelihoodDefaultDictionary,modelFile)
	modelFile.close()
	
	
# Call main function
if __name__ == '__main__':
	main()