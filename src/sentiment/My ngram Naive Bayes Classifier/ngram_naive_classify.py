from collections import defaultdict,Counter
import sys
import pickle,math

def main():
	
	if len(sys.argv) > 2:
		model_file = pickle.load( open(sys.argv[1], "rb" ))
		test_file = open(sys.argv[2], "r")
	else:
		print ("Model file and/or Training file not provided. Exiting...")
		sys.exit(0)

	#print(model_file)
	vocabsize = model_file['Sentiment_class']['vocab']

	for line in test_file:
		classname = (line.split(" ",1)[0])
		#print(classname)
		bigrams = [b for l in line.rstrip('\r\n').split(" ",1) for b in zip(l.split(" ")[:-1], l.split(" ")[1:])]
		max_class = (-1E6, '')
		for classes in (model_file['Sentiment_class'].keys()):
			if(classes != 'vocab' and classes != '\n'):
			#print(classes)
				priors = math.log(model_file['Sentiment_class'][classes])
				n = float(sum(model_file[classes].values()))
				for gram in bigrams:
					priors = priors + math.log(max(((model_file[classes][gram]+1)/(vocabsize+n)),model_file[classes][gram]/n))
				if(priors > max_class[0]):
					max_class = (priors,classes)
		print(max_class[1])
	
if __name__ == '__main__':
	main()
