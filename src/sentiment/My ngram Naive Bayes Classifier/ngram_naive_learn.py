from collections import defaultdict,Counter
import sys
import pickle

def main():

	Bigram = defaultdict(Counter)
	count = {}
	if len(sys.argv) > 2:
		training_file = open(sys.argv[1], "r")
		model_file = open(sys.argv[2], 'w')
	else:
		print ("Model file and/or Training file not provided. Exiting...")
		sys.exit(0)

	for line in training_file:
		if(line != '\n'):
			classname = (line.split(" ",1)[0]).rstrip('\n')
			#print(classname)
			bigrams = [b for l in line.rstrip('\n').split(" ",1) for b in zip(l.split(" ")[:-1], l.split(" ")[1:])]
			Bigram['Sentiment_class'][classname] += 1
			for gram in bigrams:
				if gram in count:
					count[gram] += 1
				else:
					count[gram] = 1
				Bigram[classname][gram]+=1
	#print(Bigram)
	Bigram['Sentiment_class']['vocab'] = len(count.items())
	print(Bigram)
	training_file.close()
	pickle.dump(Bigram, open(sys.argv[2], "wb") )
	model_file.close()

if __name__ == '__main__':
	main()

