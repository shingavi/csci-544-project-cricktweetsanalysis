#!/usr/bin/python3

file1 = open('a.txt','r')
file2 = open('kunal_test.txt','r')

total = 1
errNo = 0

for line1,line2 in zip(file1,file2):
	total+=1
	class1 = line1.split()[0]
	class2 = line2.split()[0]

	print(class1,class2)
	if(class1 != class2):
		errNo+=1

print("Accuracy : {}".format(float((total-errNo-1)/total * 100)))