#!/usr/bin/python3
import xlrd
from collections import Counter
import nltk
import random

data = xlrd.open_workbook('train.xls')
table = data.sheet_by_index(0)

nrows = table.nrows
ncols = table.ncols

i = 0
trainingSet = []
while i < nrows :
	rowData = table.row_values(i)
	posData = table.row_values(i+2)
	sentimentRowData = table.row_values(i+1)
	className = 0
	if(sentimentRowData[0] != 'D'):
		className = int(sentimentRowData[0])
		if(className == 1 or className == 2):
			className = 1
		if(className == 3 or className == 4):
			className = 2
		if(className == 6 or className == 7):
			className = 3
		if(className == 8 or className == 9):
			className = 4
			
	
	#print(sentimentRowData)
	id = 1
	if(className != 0):
		trainingFileWordCounter = Counter()
		while(id < len(rowData)-1):
			word = str(rowData[id])
			pos = posData[id]
			#if(sentimentRowData[id] != ''):
			if(pos!='\'' and pos != '#' and pos != '$' and pos != 'U' and pos != '^' and pos != '@' and pos != 'D' and pos != '' and pos != 'P' and pos != '&'):
				trainingFileWordCounter[word.lower()]+=1
			id = id + 1

		flag = False
		features = ' '
		for docWord in trainingFileWordCounter.keys():
			flag = True
			features += str(docWord).lower() + ' '
		if(flag):
			trainingSet.append(({'BagOfWords':features.rstrip()},str(className)))

	i = i + 6




data = xlrd.open_workbook('train.xls')
table = data.sheet_by_index(0)

nrows = table.nrows
ncols = table.ncols

i = 0
testSet = []
while i < nrows :
	rowData = table.row_values(i)
	posData = table.row_values(i+2)
	sentimentRowData = table.row_values(i+1)
	className = 0
	if(sentimentRowData[0] != 'D'):
		className = int(sentimentRowData[0])
		if(className == 1 or className == 2):
			className = 1
		if(className == 3 or className == 4):
			className = 2
		if(className == 6 or className == 7):
			className = 3
		if(className == 8 or className == 9):
			className = 4
			
	
	#print(sentimentRowData)
	id = 1
	if(className != 0):
		trainingFileWordCounter = Counter()
		while(id < len(rowData)-1):
			word = str(rowData[id])
			pos = posData[id]
			#if(sentimentRowData[id] != ''):
			if(pos!='\'' and pos != '#' and pos != '$' and pos != 'U' and pos != '^' and pos != '@' and pos != 'D' and pos != '' and pos != 'P' and pos != '&'):
				trainingFileWordCounter[word.lower()]+=1
			id = id + 1

		flag = False
		features = ' '
		for docWord in trainingFileWordCounter.keys():
			flag = True
			features += str(docWord).lower() + ' '
		if(flag):
			testSet.append(({'BagOfWords':features.rstrip()},str(className)))

	i = i + 6

random.shuffle(trainingSet)

classifier = nltk.NaiveBayesClassifier.train(trainingSet)

print(nltk.classify.accuracy(classifier,testSet))

print(classifier.show_most_informative_features())

print(classifier.classify({'BagOfWords':'live enjoy'}))