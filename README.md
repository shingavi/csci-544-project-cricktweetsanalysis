# README #

CricTwee – Tweet Analysis for the Game of Cricket

### The Project ###

The project focuses on applying NLP techniques to do analysis of relevant, unstructured data of tweets on a given match day for a game of cricket.

### Objectives ###

* Sentiment Analysis
:- To classify tweets into one of the five defined sentiment classes (Unpleasant, Sad, Neutral, Happy and Ecstatic) using Naive Bayes Approach
* Named Entity Recognition
:- To classify tweets into one of the four defined Named Entities (Player, Location, Team, Venue) using Gazettes.
* Clustering of Tweets
:- To form clusters of tweets using k-means algorithm to identify Events in the game using Gazettes.
* Summarization of Tweets
:- To identify peaks from the tweet collection and summarize the event happened in the chunk based on tweet relevancy and rank.
